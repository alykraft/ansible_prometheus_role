# Ansible prometheus role

This ansible playbook will install monitoring stack on every node in the inventory

It will perform the following action :
- Install [prometheus node exporter](https://prometheus.io/docs/guides/node-exporter/)
- Install [prometheus minecraft exporter](https://github.com/sladkoff/minecraft-prometheus-exporter) (TODO)
- Install [prometheus mysql exporter](https://github.com/prometheus/mysqld_exporter) (TODO)
- Generate prometheus configuration file
- Deploy a prometheus/grafana

## Running the playbook

From a local machine on all node

    pip install -r requirements.txt
    ansible-galaxy install cloudalchemy.grafana
    ansible-playbook -i inventory site.yml

From a local machine limiting the execution

    pip install -r requirements.txt
    ansible-galaxy install cloudalchemy.grafana
    ansible-playbook -i inventory site.yml --limit saly.infra.alykraft.fr

## Consult the grafana instance

You can consult grafana in http://valy.infra.alykraft.fr:3000

Login/Password can be found in group_vars/all.yml